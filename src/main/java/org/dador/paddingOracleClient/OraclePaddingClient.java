package org.dador.paddingOracleClient;


import static org.dador.paddingOracleClient.OraclePaddingClient.BLOCK_SIZE;
import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.net.URISyntaxException;

import static org.dador.paddingOracleClient.HexConverters.getByteArrayFromStringHexRepresentation;
import static org.dador.paddingOracleClient.HexConverters.xorArray;
import static org.dador.paddingOracleClient.HexConverters.getStringHexRepresentationFromByteArray;

/**
 * Main Class for Padding OracleClient
 */
public class OraclePaddingClient {
    static final String ENCRYPTED_MESSAGE = "5ca00ff4c878d61e1edbf1700618fb287c21578c0580965dad57f70636ea402fa0017c4acc82717730565174e2e3f713d3921bab07cba15f3197b87976525ce4";
    static final int BLOCK_SIZE = 16;

    /**
     * Fonction takes a number and creates a block of x00 values, padded according to PKCS#7
     * example : n=3 result is 00 00 .. 00 03 03 03
     * @param n : number of bytes of padding
     * @return byte[BLOCK_SIZE] filled with 0 and padding values
     */
    protected static  byte[] getPaddingArray(int n) {
        byte[] result = new byte[BLOCK_SIZE];

        int j;
       	for(j=0;j<BLOCK_SIZE-n;j++)
        {
        	result[j]=0;
        }
       	for(j=BLOCK_SIZE-n;j<BLOCK_SIZE;j++)
        {
    	 result[j]= (byte)n;
    	}
        
        return result;
    }

    /**
     * Function that create a modified ciphertext bloc for trying a guess
     * Note that the "ciphertext" correspond to the IV part for the Block Cipher
     * @param ciphertext : original ciphertext bloc
     * @param decoded    : decrypted part of the plain text (for next bloc)
     * @param position   : position of the byte to guess
     * @param guess      : the guess for this query
     * @return a byte array with c0...c(i-1)||ci+i+g||cj+mj+i||...||cn+mn+i
     */
    protected static  byte[] buildGuessForPosition(byte[] ciphertext, byte[] decoded, int position, byte guess) {
        byte[] result = new byte[BLOCK_SIZE];
        byte[] P= getPaddingArray(position);
       /* for (i=0;i<ciphertext.length;i++)
        {
            result[i] = (byte) (ciphertext[i] ^ P[i] ^ guess);
        }*/   
        
       result=xorArray(ciphertext,P );
       result[BLOCK_SIZE-1]= (byte)(result[BLOCK_SIZE-1] ^ guess);
       
       return result;
    }

    /**
     * Fonction that splits a message into constituent blocs of BLOCK_SIZE
     *
     * @param message
     * @return an array of blocs
     * @throws IllegalArgumentException
     */
    
	protected byte[][] splitMessageIntoBlocks(byte[] message) throws IllegalArgumentException {
        if (message.length % BLOCK_SIZE != 0) {
            throw new IllegalArgumentException("Message length is not a multiple of bloc size");
        }
            int i,j,nblock;
            
            nblock=message.length/ BLOCK_SIZE;
            byte[][] block = new byte[nblock][BLOCK_SIZE] ;
            for(i=0;i<nblock;i++)
            {       
            	for(j=0;j<BLOCK_SIZE;j++)
            	{
            	   block[i][j]=message[i*BLOCK_SIZE+j];
            	}
            }
        
            return block ;
    }

    /**
     * Function that takes the 2 last blocks of the message
     * and returns the length of the padding.
     * @param poq : a PaddingOracleQuery object
     * @param previousbloc : next to last block of the ciphertext
     * @param lastbloc : last bloc of the ciphertext
     * @return an integer corresponding to padding length
     * @throws IOException
     * @throws URISyntaxException
     */
    public int getPaddingLengthForLastBlock(PaddingOracleQuery poq, byte[] previousbloc, byte[] lastbloc) throws IOException, URISyntaxException {
        /**
         * TODO : Your Code HERE
         */
        // should not arrive here !
        return 0;
    }

    /**
     * Main function that takes 2 consecutive blocks of the ciphertext
     * and returns the decryption of the 2nd message block
     *
     * @param poq : a PaddingOracleQuery object to query server
     * @param iv : the "iv" part of the 2 blocks query
     * @param ciphertext : the block that will be decrypted
     * @param padding : set to 0 if not the last block. Set to paddinglength if last block
     * @return a decrypted byte array
     * @throws IOException
     * @throws URISyntaxException
     */
    public byte[] runDecryptionForBlock(PaddingOracleQuery poq, byte[] iv, byte[] ciphertext, int padding) throws IOException, URISyntaxException {
        byte[] decoded = new byte[BLOCK_SIZE];
        if (padding > 0) {
            decoded = getPaddingArray(padding);
        }
        
        
        /**
         * TODO : YOUR CODE HERE
         */
        return decoded;
    }

    public static void main(String[] args) throws IOException, URISyntaxException {
        OraclePaddingClient opc = new OraclePaddingClient();
        PaddingOracleQuery opq = new PaddingOracleQuery();
        try {
            System.out.println("Server responded : " + opq.query(ENCRYPTED_MESSAGE));
        } catch (Exception e) {
            System.out.print("Exception caught. Server down ?");
            e.printStackTrace();
        }
        try {
            /**
             * TODO : YOUR CODE HERE
             */
        byte[] hexMessage = getByteArrayFromStringHexRepresentation(ENCRYPTED_MESSAGE);
        byte[][] result = opc.splitMessageIntoBlocks(hexMessage);
        byte [] cypher= result[0];
        byte [] Guess;
        String str1,str2,str;
        byte[] decoder =new byte[BLOCK_SIZE];
        for(byte guess = 0; guess<256; guess++)
        { 
        	Guess=OraclePaddingClient.buildGuessForPosition(cypher, decoder, 1, guess);
        	str1=getStringHexRepresentationFromByteArray(Guess);
        	str2=getStringHexRepresentationFromByteArray(result[1]);
        	str=str1.concat(str2);
        	opq.query(str);
        } 
        
        } catch (Exception e) {
            System.out.print("Exception caught. Server down ?");
            e.printStackTrace();
        }
        
 
        }

}

